
#include "SFML/Graphics.hpp"

using namespace sf;

/*
SFML super skinny skeleton starter app
*/
int main()
{
	// Create the main window
	RenderWindow window(VideoMode(1200, 800), "Hello world!");

	// Start the game loop
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed)
				window.close();
		} 

		// Clear screen
		window.clear();

		CircleShape circle;
		circle.setRadius(50);
		circle.setPosition(100, 100);
		circle.setFillColor(Color::Blue);
		window.draw(circle);

		RectangleShape rect;
		Vector2f sz{ 100,75 };
		rect.setSize(sz);
		rect.setPosition(200, 200);
		rect.setFillColor(Color::Red);
		window.draw(rect);

		circle.setRadius(75);
		circle.setPointCount(3);
		circle.setPosition(300, 300);
		circle.setFillColor(Color::Cyan);
		window.draw(circle);

		ConvexShape convex;
		convex.setPointCount(5);
		convex.setPoint(0, Vector2f(0, 0));
		convex.setPoint(1, Vector2f(150, 10));
		convex.setPoint(2, Vector2f(120, 90));
		convex.setPoint(3, Vector2f(30, 100));
		convex.setPoint(4, Vector2f(0, 50));
		convex.setPosition(500, 500);
		convex.setFillColor(Color::Green);
		window.draw(convex);

		convex.setPosition(500, 100);
		convex.setFillColor(Color::Magenta);
		convex.setScale(2.f, 2.f);
		window.draw(convex);

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
